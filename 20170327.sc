import scala.util.Random

val ms:Map[String,Int]=Map("x"->24,"y"->25,"z"->26)
ms("x")
ms.contains("x")
ms.contains("a")
ms+("a"->1)
ms+(("a",1))

ms.mapValues(v=>v+100)
ms.map(kv=>kv._2+100)
ms.map(kv=>kv._1->(kv._2+100))

val ms1=Map("Mark"->1,"Tom"->2,"HI"->3)
ms1.filterKeys(str=>str.length<3)

List(1,2,3).filter(_>2)
List("Mark","Tom","HI").filter(_.length<3)

def groupByFirstChar(strs:List[String])={
  strs.groupBy(str=>str.head)
}
groupByFirstChar(List("Mark","Mike","Tom"))

val list=List(2->"c",1->"d",4->"a",3->"b")
list.sortBy(kv=>{
  println(kv)
  kv._1
})//作排序，會有迭代->

def groupBylsOdd(nums:List[Int])={
  nums.groupBy(num=>num%2==1)
}
groupBylsOdd(List(1,2,3))

def sortByKey(kvs:List[(Int,String)])={
  kvs.sortBy(kv=>kv._1)
}
sortByKey(List(2->"c",1->"d",4->"a",3->"b"))

val rnd=Random //或是scala.util.Random
(1 to 52)
List(1 to 10) //XX
(1 to 10).toList //OO

val nums=(1 to 52).toList //toList表示把前面的轉成List類型
nums.sortBy(i=>rnd.nextInt)
nums.sortBy(i=>rnd.nextInt).take(10)

List(10,20,30,40).reduce((acc,x)=>{
  println("acc"+acc+"+"+x)
  acc+x
})
List(10,20,30,40).reduce((acc,x)=>acc+x)
List(10,20,30,40).reduce(_+_)
List(10,20,30,40).map(_+1) //redeuce和map一樣，看考試老師指定用哪個

List("Hello","I","am","Mark").reduce((acc,x)=>{
  acc+" "+x
})
List("Hello","I","am","Mark").mkString(" ") //reduce和makeString的意思一樣，看考試老師要哪個
//List("HI","Mark").foldLeft(0)((acc,x)=>acc+ " "+x)

List(1,2,3,4,5)
def accumulator(nums:List[Int])={
  nums.foldLeft(List(0))((acc,x)=>{
    println(acc)
    acc :+ (acc.last+x)
  }).tail
}
accumulator(List(1,2,3,4,5))

val words=List("Apple","Banana","Apple","Cherry","Apple")
words.groupBy(v=>v).mapValues(_.length)

